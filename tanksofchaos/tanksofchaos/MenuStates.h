//
//  MenuStates.h
//  tanks_of_chaos
//
//  Created by Ashok on 5/27/13.
//  Copyright (c) 2013 MAD. All rights reserved.
//

#ifndef __tanks_of_chaos__MenuStates__
#define __tanks_of_chaos__MenuStates__

class MenuManager;
template <class MenuManager> class State;

//------------------------------------------------------------------------------
class MainMenuScreen: public State<MenuManager>{
private:
    MainMenuScreen(){}
    
    MainMenuScreen(MainMenuScreen const&);
    
    
public:
    
    //~MainMenuScreen(){}
    
    static MainMenuScreen* getInstance();
    
    void Enter(MenuManager*);
    
    void Execute(MenuManager*);
    
    void Exit(MenuManager*);
    
};


//------------------------------------------------------------------------------
class EnterSinglePlayerMode: public State<MenuManager>{
private:
    EnterSinglePlayerMode(){}
    
    EnterSinglePlayerMode(EnterSinglePlayerMode const&);
    
    
public:
    
    static EnterSinglePlayerMode* getInstance();
    
    void Enter(MenuManager*);
    
    void Execute(MenuManager*);
    
    void Exit(MenuManager*);

};

//------------------------------------------------------------------------------

class EnterDoublePlayerMode: public State<MenuManager>{
private:
    EnterDoublePlayerMode(){}
    
    EnterDoublePlayerMode(EnterDoublePlayerMode const&);
    
    
public:
    
    static EnterDoublePlayerMode* getInstance();
    
    void Enter(MenuManager*);
    
    void Execute(MenuManager*);
    
    void Exit(MenuManager*);
};

//------------------------------------------------------------------------------
class EnterInstructionsMenu: public State<MenuManager>{
private:
    EnterInstructionsMenu(){}
    
    EnterInstructionsMenu(EnterInstructionsMenu const&);
    
    
public:
    
    static EnterInstructionsMenu* getInstance();
    
    void Enter(MenuManager*);
    
    void Execute(MenuManager*);
    
    void Exit(MenuManager*);
    
};


//------------------------------------------------------------------------------
class EnterCreditsMenu: public State<MenuManager>{
private:
    EnterCreditsMenu(){}
    
    EnterCreditsMenu(EnterCreditsMenu const&);
    
    
public:
    
    static EnterCreditsMenu* getInstance();
    
    void Enter(MenuManager*);
    
    void Execute(MenuManager*);
    
    void Exit(MenuManager*);
    
};
//
//------------------------------------------------------------------------------
class ExitGame: public State<MenuManager>{
private:
    ExitGame(){}
    
    ExitGame(ExitGame const&);
    
    
public:
    
    //~ExitGame() {}
    
    static ExitGame* getInstance();
    
    void Enter(MenuManager*);
    
    void Execute(MenuManager*);
    
    void Exit(MenuManager*);
    
};

#endif /* defined(__tanks_of_chaos__MenuStates__) */
