//
//  MenuStates.cpp
//  tanks_of_chaos
//
//  Created by Ashok on 5/27/13.
//  Copyright (c) 2013 MAD. All rights reserved.
//

#include <iostream>
#include "States.h"
#include "MenuStates.h"
#include "MenuManager.h"
#include "EngineSingletons.h"
#include "GallarySingletons.h"


//------------------------------------------------------------------------------
//Definitions for Mainmenu Screen state
MainMenuScreen* MainMenuScreen::getInstance(){
    static MainMenuScreen instance;
    
    return &instance;
}

void MainMenuScreen::Enter(MenuManager *manager){
    std::cout << "Entering Main Menu Screen\n";
}

void MainMenuScreen::Execute(MenuManager *manager){
    std::cout << "Entered Main Menu\n";
    
    bool *keystate = in->GetKeyState();
    while (true) {
        in->ProcessEvents();
        if (keystate[SDLK_1]){
            while (keystate[SDLK_1])
                in->ProcessEvents();
            manager->GetFSM()->ChangeState(EnterSinglePlayerMode::getInstance());
            break;
        }
        
        if (keystate[SDLK_2]){
            while (keystate[SDLK_2])
                in->ProcessEvents();
            manager->GetFSM()->ChangeState(EnterDoublePlayerMode::getInstance());
            break;
        }
        
        if (keystate[SDLK_3]){
            while (keystate[SDLK_3])
                in->ProcessEvents();
            manager->GetFSM()->ChangeState(EnterInstructionsMenu::getInstance());
            break;
        }
        
        if (keystate[SDLK_4]){
            while (keystate[SDLK_4])
                in->ProcessEvents();
            manager->GetFSM()->ChangeState(EnterCreditsMenu::getInstance());
            break;
        }

        if (keystate[SDLK_5]){
            while (keystate[SDLK_5])
                in->ProcessEvents();
            manager->GetFSM()->ChangeState(ExitGame::getInstance());
            break;
        }
        gfx->ClearScreen();
        gfx->PutImage(igal.getMenuBgImage(), 0, 0);
        gfx->FlipDrawing();
        gfx->Wait(50);
    }
}

void MainMenuScreen::Exit(MenuManager *manager){
    std::cout << "Exiting Main Menu\n";
}


//------------------------------------------------------------------------------
//Definitions for EnterSingle Player state 
EnterSinglePlayerMode* EnterSinglePlayerMode::getInstance(){
    static EnterSinglePlayerMode instance;
    
    return &instance;
}

void EnterSinglePlayerMode::Enter(MenuManager *manager){
    std::cout << "Entering Singl Player Mode\n";
}

void EnterSinglePlayerMode::Execute(MenuManager *manager){
    std::cout << "Entered single Player Mode\n";
    
    bool *keystate = in->GetKeyState();
    while (true) {
        in->ProcessEvents();
        if (keystate[SDLK_ESCAPE]){
            while (keystate[SDLK_ESCAPE]){
                in->ProcessEvents();
            }
            break;
        }
        gfx->ClearScreen();
        gfx->PutImage(igal.getMenuPointerImage(), 0, 0);
        gfx->FlipDrawing();
        gfx->Wait(50);
    }
    manager->GetFSM()->ChangeState(MainMenuScreen::getInstance());
}

void EnterSinglePlayerMode::Exit(MenuManager *manager){
    std::cout << "Exiting single player mode\n";
}

//------------------------------------------------------------------------------
//Definition for Enter Double Player state
EnterDoublePlayerMode* EnterDoublePlayerMode::getInstance(){
    static EnterDoublePlayerMode instance;
    
    return &instance;
}

void EnterDoublePlayerMode::Enter(MenuManager *manager){
    std::cout << "Entering Double Player Mode\n";
}

void EnterDoublePlayerMode::Execute(MenuManager *manager){
    std::cout << "Entered double Player Mode\n";
    
    bool *keystate = in->GetKeyState();
    while (true) {
        in->ProcessEvents();
        if (keystate[SDLK_ESCAPE]){
            while (keystate[SDLK_ESCAPE]){
                in->ProcessEvents();
            }
            break;
        }
        gfx->ClearScreen();
        gfx->PutImage(igal.getWelcomeImage(), 0, 0);
        gfx->FlipDrawing();
        gfx->Wait(50);
    }
    manager->GetFSM()->ChangeState(MainMenuScreen::getInstance());
}

void EnterDoublePlayerMode::Exit(MenuManager *manager){
    std::cout << "Exiting double player mode\n";
}


//------------------------------------------------------------------------------
//Definition of Enter Instructions state

EnterInstructionsMenu* EnterInstructionsMenu::getInstance(){
    static EnterInstructionsMenu instance;
    
    return &instance;
}

void EnterInstructionsMenu::Enter(MenuManager *manager){
    std::cout << "Entering Instructions menu\n";
}

void EnterInstructionsMenu::Execute(MenuManager *manager){
    std::cout << "Entered Instructions\n";
    
    bool *keystate = in->GetKeyState();
    while (true) {
        in->ProcessEvents();
        if (keystate[SDLK_ESCAPE]){
            while (keystate[SDLK_ESCAPE]){
                in->ProcessEvents();
            }
            break;
        }
        gfx->ClearScreen();
        gfx->PutImage(igal.getInstructionsImage(), 0, 0);
        gfx->FlipDrawing();
        gfx->Wait(50);
    }
    manager->GetFSM()->ChangeState(MainMenuScreen::getInstance());
}

void EnterInstructionsMenu::Exit(MenuManager *manager){
    std::cout << "Exiting Instructoins\n";
}


//------------------------------------------------------------------------------
//Definition of Enter Credits State

EnterCreditsMenu* EnterCreditsMenu::getInstance(){
    static EnterCreditsMenu instance;
    
    return &instance;
}

void EnterCreditsMenu::Enter(MenuManager *manager){
    std::cout << "Entering Credits menu \n";
}

void EnterCreditsMenu::Execute(MenuManager *manager){
    std::cout << "Entered credite menu\n";
    
    bool *keystate = in->GetKeyState();
    while (true) {
        in->ProcessEvents();
        if (keystate[SDLK_ESCAPE]){
            while (keystate[SDLK_ESCAPE]){
                in->ProcessEvents();
            }
            break;
        }
        gfx->ClearScreen();
        gfx->PutImage(igal.getCreditsImage(), 0, 0);
        gfx->FlipDrawing();
        gfx->Wait(50);
    }
    manager->GetFSM()->ChangeState(MainMenuScreen::getInstance());
}

void EnterCreditsMenu::Exit(MenuManager *manager){
    std::cout << "Exiting credits menu\n";
}

//------------------------------------------------------------------------------
//Definition of Enter Quit State

ExitGame* ExitGame::getInstance(){
    static ExitGame instance;
    
    return &instance;
}

void ExitGame::Enter(MenuManager *manager){
    std::cout << "Entering exit\n";
}

void ExitGame::Execute(MenuManager *manager){
    std::cout << "exiting...\n";
    
    //gameover = true;
}

void ExitGame::Exit(MenuManager *manager){
    std::cout << "i'm exitting\n";
}



