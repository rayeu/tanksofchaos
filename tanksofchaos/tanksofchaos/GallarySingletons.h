//
//  GallarySingletons.h
//  tanks_of_chaos
//
//  Created by Ashok on 5/28/13.
//  Copyright (c) 2013 MAD. All rights reserved.
//

#ifndef tanks_of_chaos_GallarySingletons_h
#define tanks_of_chaos_GallarySingletons_h

#include "ImageGallary.h"
#include "SoundGallary.h"

#define igal ImageGallary::getInstance()
#define sgal SoundGallary::getInstance()

#endif
