
#ifndef tanks_of_chaos_SoundGallary_h
#define tanks_of_chaos_SoundGallary_h

#include "SDLHeaders.h"
#include "EngineSingletons.h"

class SoundGallary{
    /*
     This is a gallary of sounds used in the game.
     It will be used as a singelton.
     */
private:
    Mix_Chunk *mainMenuUpDownClip;
    Mix_Chunk *mainMenuSelectionClip;
        
    SoundGallary(){}
    SoundGallary (SoundGallary const&);
    
public:
    
    static SoundGallary& getInstance(){
        static SoundGallary instance;
        
        return instance;
    }

    void LoadSounds(){
        //Loading sounds
        if (!mainMenuUpDownClip) mainMenuUpDownClip = sfx->LoadSample("sfx/laser.wav");
        if (!mainMenuSelectionClip) mainMenuSelectionClip = sfx->LoadSample("sfx/button.wav");
    }
    
    ~SoundGallary(){
        sfx->FreeSample(mainMenuUpDownClip);
        sfx->FreeSample(mainMenuSelectionClip);
    }
    
    inline Mix_Chunk* getMainMenuUpDownClip(){
        return mainMenuUpDownClip;
    }
    
    inline Mix_Chunk* getMainMenuSelectionClip(){
        return mainMenuSelectionClip;
    }

};

#endif
