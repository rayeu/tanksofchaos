//
//  Singletons.h
//  tanks_of_chaos
//
//  Created by Ashok on 5/28/13.
//  Copyright (c) 2013 MAD. All rights reserved.
//

#ifndef tanks_of_chaos_Singletons_h
#define tanks_of_chaos_Singletons_h

#include "Engine.h"


#define gfx Engine::getInstance().graphicsEngine()
#define sfx Engine::getInstance().soundEngine()
#define in Engine::getInstance().inputEngine()

#define engine Engine::getInstance()

#endif
