//
//  MenuManager.h
//  tanks_of_chaos
//
//  Created by Ashok on 5/27/13.
//  Copyright (c) 2013 MAD. All rights reserved.
//

#ifndef __tanks_of_chaos__MenuManager__
#define __tanks_of_chaos__MenuManager__

#include "StateMachine.h"
#include "MenuStates.h"

class MenuManager{
private:
    StateMachine<MenuManager>* stateMachine;
    
public:
    MenuManager(){
        stateMachine = new StateMachine<MenuManager>(this);
        
        stateMachine->SetCurrentState(MainMenuScreen::getInstance());
    }
    
    ~MenuManager(){
        delete stateMachine;
    }
    
    void Update(){
        stateMachine->Update();
    }
    
    StateMachine<MenuManager>* GetFSM() const{
        return stateMachine;
    }
};

#endif /* defined(__tanks_of_chaos__MenuManager__) */
