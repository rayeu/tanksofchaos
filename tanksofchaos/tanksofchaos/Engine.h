
#ifndef tanks_of_chaos_Engine_h
#define tanks_of_chaos_Engine_h

#include "SDLHeaders.h"

class Engine{
private:
    SDL_Graphics *gfx = NULL;
    SDL_Sound *sfx = NULL;
    SDL_Input *input = NULL;
    
    Engine(){}
    Engine(Engine const&);
    
public:
    static Engine& getInstance(){
        static Engine instace;
        
        return instace;
    }
    
    void initializeComponents(SDL_Graphics *gfx, SDL_Sound *sfx, SDL_Input *input){
        this->gfx = gfx;
        this->sfx = sfx;
        this->input = input;
    }
    
    SDL_Input* inputEngine(){
        return this->input;
    }
    
    SDL_Sound* soundEngine(){
        return this->sfx;
    }
    
    SDL_Graphics* graphicsEngine(){
        return this->gfx;
    }
    
};


#endif
