
#include "SDLHeaders.h"
#include "MenuManager.h"

#include "EngineSingletons.h"
#include "GallarySingletons.h"

int main(int argc, char* argv[])
{
    SDL_Graphics *graphics = new SDL_Graphics(640, 480, 32, SDL_HWSURFACE|SDL_DOUBLEBUF);
    SDL_Input *input = new SDL_Input;
    SDL_Sound *sound = new SDL_Sound(22050);
    
    //Sigletons
    engine.initializeComponents(graphics, sound, input);
    igal.LoadImages();
    sgal.LoadSounds();
    
    MenuManager *menuManager = new MenuManager();
    
    bool gameover = false;
    while (!gameover) {
        menuManager->Update();
    }
    
    delete menuManager;
    free(graphics);
    free(sound);
    free(input);
    
    return 0;
}





