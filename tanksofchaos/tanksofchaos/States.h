//
//  States.h
//  tanks_of_chaos
//
//  Created by Ashok on 5/27/13.
//  Copyright (c) 2013 MAD. All rights reserved.
//

#ifndef tanks_of_chaos_States_h
#define tanks_of_chaos_States_h

template <class T>
class State {
public:
    
    virtual ~State(){}
    
    virtual void Enter(T*) = 0;
    
    virtual void Execute(T*) = 0;
    
    virtual void Exit(T*) = 0;
};


#endif
