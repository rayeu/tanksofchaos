#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "States.h"
#include <cstddef>

template <class T>
class StateMachine
{
private:
	T *Owner;

	State<T> *currentState;

	State<T> *previousState;

	State<T> *globalState;

public:
	StateMachine(T *owner): Owner(owner),
                            currentState(NULL),
                            globalState(NULL),
                            previousState(NULL)
    {}

	void SetCurrentState(State<T> *s) {currentState = s;}

	void SetGlobalState(State<T> *s) {globalState = s;}

	void ChangeState(State<T> *newState){
        previousState = currentState;
        
        currentState->Exit(Owner);
        
        currentState = newState;
        
        currentState->Enter(Owner);
    }

    void RevertToPreviousState(){
        this->ChangeState(previousState);
    }

    void Update(){
        if(globalState) globalState->Execute(Owner);
        
        if(currentState) currentState->Execute(Owner);
    }

	bool isInState(State<T> &st) {
        if (currentState == st)
            return true;
        return false;
    }

	State<T>* GetCurrentState()const {return currentState;}
	State<T>* GetPreviousState()const {return previousState;}
	State<T>* GetGlobalState() const {return globalState;}
};

#endif