
#ifndef tanks_of_chaos_ImageGallary_h
#define tanks_of_chaos_ImageGallary_h

#include "SDLHeaders.h"
#include "EngineSingletons.h"

class ImageGallary{
    /*
     This is a gallary of images used in the game.
     It should be used as a singleton.
     */
private:
    SDL_Surface* welcomeImage;
    SDL_Surface* menuPointerImage;
    SDL_Surface* menuBgImage;
    SDL_Surface* level;
    SDL_Surface* instructionsImage;
    SDL_Surface* creditsImage;
    
    ImageGallary() {}
    ImageGallary(ImageGallary const&);
    
public:
    static ImageGallary& getInstance(){
        static ImageGallary instance;
        
        return instance;
    }
    
    void LoadImages(){
        
        //Loads all the required image files.
        if (!welcomeImage) welcomeImage = gfx->LoadImage("gfx/EnemyTanks.png");
        if (!menuPointerImage) menuPointerImage = gfx->LoadImageAlpha("gfx/menu/Pointer.png" );
        if (!menuBgImage) menuBgImage = gfx->LoadImageAlpha("gfx/menu/MenuScreen1.png");
        if (!level) level = gfx->LoadImageAlpha("gfx/menu/ScoreBoard.png");
        if (!instructionsImage) instructionsImage = gfx->LoadImageAlpha("gfx/menu/Instructions.png");
        if (!creditsImage) creditsImage = gfx->LoadImageAlpha("gfx/menu/Credits.png");
    }
    
    ~ImageGallary(){
        gfx->FreeImage(welcomeImage);
        gfx->FreeImage(menuPointerImage);
        gfx->FreeImage(menuBgImage);
        gfx->FreeImage(level);
        gfx->FreeImage(instructionsImage);
        gfx->FreeImage(creditsImage);
    }
    
    inline SDL_Surface* getWelcomeImage(){
        return welcomeImage;
    }
    
    inline SDL_Surface* getMenuPointerImage(){
        return menuPointerImage;
    }
    
    inline SDL_Surface* getMenuBgImage(){
        return menuBgImage;
    }
    
    inline SDL_Surface* getInstructionsImage(){
        return instructionsImage;
    }
    
    inline SDL_Surface* getCreditsImage(){
        return creditsImage;
    }
};


#endif
